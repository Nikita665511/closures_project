const cacheFunction = require('../ cacheFunction.cjs');
cb = x => `${x} is used first time!`

let fun = cacheFunction(cb);
console.log(fun("mydata"));
console.log(fun("mydata"));
console.log(fun("data"));
console.log(fun("data"));
