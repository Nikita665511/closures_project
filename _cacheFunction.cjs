
function cacheFunction(cb) {
    var cache = {};
    return function(argument) {
        if (cache.hasOwnProperty(argument)) {
            return cache;
        } else {
            cache[argument]=cb(argument);
            return cb(argument);
        }
    }
}
module.exports = cacheFunction;

