function limitFunctionCallCount(cb, n) {
    let limit = n;
    function limitFunction()
    {
        if (limit > 0) 
        {
            limit--;
            return cb();
        }
        else
            return null;
    }
    return limitFunction;
    }
module.exports = limitFunctionCallCount;  