function counterFactory() {
        let counter = 1;
    
        const increment = () => {
            return ++counter;
        }
        const decrement = () => {
            return --counter;
        }
    
        return {
            increment,
            decrement
        }
    }
module.exports = counterFactory;
    

